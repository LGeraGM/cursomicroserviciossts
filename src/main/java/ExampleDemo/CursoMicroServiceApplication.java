package ExampleDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CursoMicroServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CursoMicroServiceApplication.class, args);
	}

}
