package ExampleDemo.user;

import org.springframework.stereotype.Component;
import java.util.*;


@Component
public class UserDaoService {
private static List<User> users = new ArrayList<>();

private static int userCount=3;

static {
	users.add(new User(1, "Adam", new Date()));
	users.add(new User(2, "Eve", new Date()));
	users.add(new User(3, "Jack", new Date()));
}

// find all user
public List<User> findAll(){
	return users;
}

// save user
public User save(User user) {
	if(user.getId()==null) {
		user.setId(++userCount);
	}
	users.add(user);
	return user;
}

// find only one user
public User findOne(int id) {
	for(User user:users) {
		if(user.getId()==id) {
			return user;
		}
	}
	return null;
}

// Delete user
public User deleteById(int id) {
	Iterator<User> iterator = users.iterator();
	while(iterator.hasNext()) {
		User user = iterator.next();
		if(user.getId()==id) {
			iterator.remove();
			return user;
		}
	}
	return null;
}

}
