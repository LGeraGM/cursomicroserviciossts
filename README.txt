# Curso Microservicios con Spring Boot

## Contenido

1. app default

2. helloworld
2.1 hello-world
2.2 hello-world-bean
2.3 hello-world/path-variable/valor

3. user
3.1 GET/users
3.2 GET/users/id
3.3 POST/users
3.4 DELETE/users/id

4. posts
4.1 GET/jpa/users/id/posts
4.2 POST/jpa/users/id/posts